FROM ubuntu

# Update aptitude with new repo
RUN apt-get update

# Install software 
RUN apt-get install -y git

ADD PC1.ps1 C:\Users\Peter\Documents\test12\PC1.ps1

CMD powershell C:\Users\Peter\Documents\test12\PC1.ps1 ;
